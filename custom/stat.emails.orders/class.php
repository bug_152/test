<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc;


/**
* Компонент с доступом только для администраторов, в котором выводится информация по количеству заказов,
* сгруппированных по доменному имени почты.
*/
class StatEmailsOrdersComponent extends CBitrixComponent
{
    protected $errors = array(); //Список ошибок

    public function __construct($component = null)
    {
        parent::__construct($component);

        Loc::loadMessages(__FILE__);

        if (!Loader::includeModule('sale'))
        {
            $this->errors[] = Loc::getMessage('SALE_MODULE_NOT_INSTALLED');
        };

        global $USER;
        if(!$USER->IsAdmin())
        {
            $this->errors[] = Loc::getMessage('NEED_ADMIN_RIGHTS');
        }
    }

    /**
     * Подготовка входных параметров
     */
    public function onPrepareComponentParams($arParams)
    {
        if(empty($arParams["SORT_BY"]) || !in_array(strtoupper($arParams["SORT_BY"]), array('DOMAIN','CNT')))
            $arParams["SORT_BY"] = "CNT";

        if(empty($arParams["SORT_ORDER"]) || !in_array(strtoupper($arParams["SORT_ORDER"]), array('ASC','DESC')))
            $arParams["SORT_ORDER"] = "ASC";

        return $arParams;
    }

    /**
     * Получаем статистику заказов по доменам
     */
    protected function GetStatEmails()
    {
        $res = array();

        $arSelect = array(
            new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(*)'),
            new \Bitrix\Main\Entity\ExpressionField('DOMAIN', "(SUBSTR(%1\$s, INSTR(%1\$s, '@') + 1))", array('PROPERTY_EMAIL.VALUE')),
        );

        $arRuntime = array(
            "PROPERTY_EMAIL" => array(
                'data_type' => '\Bitrix\Sale\Internals\OrderPropsValueTable',
                'reference' => array(
                    '=this.ID' => 'ref.ORDER_ID',
                    '=ref.CODE' => new \Bitrix\Main\DB\SqlExpression('?', 'EMAIL'),
                )),
        );

        $dbOrders = \Bitrix\Sale\Internals\OrderTable::getList(array(
            'order' => array($this->arParams["SORT_BY"] => $this->arParams["SORT_ORDER"]),
            'filter' => array('!DOMAIN' => ''),
            'select' => $arSelect,
            'group' => array('DOMAIN'),
            'runtime' => $arRuntime,
        ));

        if($dbOrders->getSelectedRowsCount() > 0)
        {
            while ($arOrder = $dbOrders->fetch())
            {
                $res[$arOrder["DOMAIN"]] = $arOrder["CNT"];
            }
        }
        else
        {
            $this->errors[] = Loc::getMessage('ORDERS_NOT_FOUND');
        }
        
        return $res;
    }

    /**
     * Точка входа
     */
    public function executeComponent()
    {
        if(empty($this->errors))
        {
            $this->arResult['ORDERS'] = $this->GetStatEmails();
        }

        $this->arResult['ERRORS'] = $this->errors;

        $this->includeComponentTemplate();
    }
}