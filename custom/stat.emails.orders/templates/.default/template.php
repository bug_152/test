<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<?
if (!empty($arResult['ERRORS']))
{
    foreach ($arResult['ERRORS'] as $error)
    {
        ShowError($error);
    }
}
else
{
    ?>
    <div class="table-stat">
        <div class="table-stat-row caption">
            <div><?=Loc::getMessage('STAT_EMAILS_DOMAIN')?></div>
            <div><?=Loc::getMessage('STAT_EMAILS_CNT')?></div>
        </div>
    <?
    foreach($arResult['ORDERS'] as $domain=>$count)
    {
        ?>
            <div class="table-stat-row">
                <div><?=$domain?></div>
                <div><?=$count?></div>
            </div>
        <?
    }
    ?>
    </div>
<?
}
?>
