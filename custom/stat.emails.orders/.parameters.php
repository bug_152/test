<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

$arSorts = array("ASC"=>Loc::getMessage("STAT_EMAILS_ORDERS_COMPONENT_ASC"), "DESC"=>Loc::getMessage("STAT_EMAILS_ORDERS_COMPONENT_DESC"));
$arSortFields = array(
    "DOMAIN"=>Loc::getMessage("STAT_EMAILS_ORDERS_COMPONENT_DOMAIN"),
    "CNT"=>Loc::getMessage("STAT_EMAILS_ORDERS_COMPONENT_CNT"),
);


$arComponentParameters = array(
    "PARAMETERS" => array(
        "SORT_BY" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("STAT_EMAILS_ORDERS_COMPONENT_BY"),
            "TYPE" => "LIST",
            "DEFAULT" => "CNT",
            "VALUES" => $arSortFields,
        ),
        "SORT_ORDER" => array(
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("STAT_EMAILS_ORDERS_COMPONENT_ORDER"),
            "TYPE" => "LIST",
            "DEFAULT" => "ASC",
            "VALUES" => $arSorts,
        ),
    )
);
?>