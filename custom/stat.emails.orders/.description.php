<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage("STAT_EMAILS_ORDERS_COMPONENT_NAME"),
	"DESCRIPTION" => Loc::getMessage("STAT_EMAILS_ORDERS_COMPONENT_DESCR"),
	"PATH" => array(
		"ID" => "e-store",
		"CHILD" => array(
				"ID" => "custom",
				"NAME" => Loc::getMessage("CUSTOM_GROUP_NAME")
			),
		),
);
?>